function [trans_rate] = RFQ_solver(RF,RFQ,plot_flag)


%% SWEEP
[CB,RF] = sweep(RFQ,RF); % Create PARIOUT Channel Block
[E,Es] = assess_CB(CB,RF,RFQ,false); % Evaluate Initial Channel block

%% CALL PARMTEQ
CB_Pteq = wrap_Pteq(CB, RF, RFQ, true); % Write RFQ.IN4 input file
% run pteq
if plot_flag
    system("cd C:\Users\nyquist\Desktop\EHU-BOX\HALT\Pteq & start RFQ.IN4 & auto_pari.py");
%     pause(1)
    system("cd C:\Users\nyquist\Desktop\EHU-BOX\HALT\Pteq & Parmteqm.lnk");
    system("cd C:\Users\nyquist\Desktop\EHU-BOX\HALT\Pteq & Lingraf.lnk");
    pause(0.5)
    [trans_rate,Ws] = read_lgraf(); % Read pteq ouputt file (discrete values)
else   
    system("cd C:\Users\nyquist\Desktop\EHU-BOX\HALT\Pteq & start RFQ.IN4 & auto_pari.py");
%    pause(1)
    system("cd C:\Users\nyquist\Desktop\EHU-BOX\HALT\Pteq & Parmteqm.lnk");
    pause(0.5)
    trans_rate = read_pteq(); % Read pteq ouputt file (discrete values)
end

%% CALL TOUTATIS
if plot_flag
    [CB_tout, Es_tout] = pteq2tout(RFQ,RF,true);
    [Ws_tout, trans_rate_tout] = read_tout_out();
end

%% PLOT

if plot_flag
    
figure('units','normalized','outerposition',[0 0 1 1])
    
    % RFQ params: user input
    subplot(2,3,1)
    plot(CB(:,1),CB(:,8)*10)
    hold on
    plot(CB(:,1),CB(:,9))
    ylim([0 4])
    yyaxis right
    plot(CB(:,1),CB(:,7),'-','Color','#EDB120')
    ylim([-40 -5])
    xline(interp1(CB(:,14),CB(:,1),RFQ(2,4)/10),':');
    xline(interp1(CB(:,14),CB(:,1),RFQ(3,4)/10),':');
    xline(interp1(CB(:,14),CB(:,1),RFQ(4,4)/10),':');
    legend('a [mm]', 'm', 'phi_s [deg]','Location','south')
    legend('Orientation','horizontal')
    xlabel('Cell number')
    title('RFQ Design Parameters (User Input)')
    hold off
    drawnow
    
    % RFQ params: Pteq modified
    subplot(2,3,4)
    plot(CB_tout(:,1),CB_tout(:,8)*10)
    hold on
    plot(CB_tout(:,1),CB_tout(:,9))
    ylim([0 4])
    yyaxis right
    plot(CB_tout(:,1),CB_tout(:,7),'-','Color','#EDB120')
    ylim([-40 -5])
    xline(interp1(CB_tout(:,14),CB_tout(:,1),RFQ(2,4)/10),':');
    xline(interp1(CB_tout(:,14),CB_tout(:,1),RFQ(3,4)/10),':');
    xline(interp1(CB_tout(:,14),CB_tout(:,1),RFQ(4,4)/10),':');
    legend('a [mm]', 'm', 'phi_s [deg]','Location','south')
    legend('Orientation','horizontal')
    xlabel('Cell number')
    title('RFQ Design Parameters (Parmteq/Toutatis)')
    hold off
    drawnow
    
    % RFQ performance: analytical approach
    subplot(2,3,2)
    plot(CB(:,1),CB(:,12))
    hold on
    plot(CB(:,1),Es.K_factor)
    xline(interp1(CB(:,14),CB(:,1),RFQ(2,4)/10),':');
    xline(interp1(CB(:,14),CB(:,1),RFQ(3,4)/10),':');
    xline(interp1(CB(:,14),CB(:,1),RFQ(4,4)/10),':');
    legend('Focusing Strength', 'Kilpatrick Factor','Location','south')
    legend('Orientation','horizontal')
    xlabel('Cell number')
    ylim([1 3.5])
    title('Beam Focusing (Analytical)')
    hold off
    
    % RFQ performance:from Pteq/Toutatis channel block
    subplot(2,3,5)
    plot(CB_tout(:,1),CB_tout(:,12))
    hold on
    plot(CB_tout(:,1),Es_tout.K_factor)
    xline(interp1(CB_tout(:,14),CB_tout(:,1),RFQ(2,4)/10),':');
    xline(interp1(CB_tout(:,14),CB_tout(:,1),RFQ(3,4)/10),':');
    xline(interp1(CB_tout(:,14),CB_tout(:,1),RFQ(4,4)/10),':');
    legend('Focusing Strength', 'Kilpatrick Factor','Location','south')
    legend('Orientation','horizontal')
    xlabel('Cell number')
    ylim([1 3.5])
    title('Beam Focusing (Simulated)')
    hold off
    
    % Acceleration & transmission rates: Pteq computed
    subplot(2,3,3)
    plot(Ws)
    ylim([-1 5.5])
    hold on
    yyaxis right
    plot(trans_rate)
    ylim([-10 105])
    xline(interp1(CB_tout(:,14),CB_tout(:,1),RFQ(2,4)/10),':');
    xline(interp1(CB_tout(:,14),CB_tout(:,1),RFQ(3,4)/10),':');
    xline(interp1(CB_tout(:,14),CB_tout(:,1),RFQ(4,4)/10),':');
    legend('Ws [MeV]', 'Transmission rate','Location','south')
    legend('Orientation','horizontal')
    xlabel('Cell number')
    title('Beam Dynamics (Parmteq)')
    hold off
    
    % Acceleration & transmission rates: Toutatis computed
    subplot(2,3,6)
    plot(Ws_tout)
    ylim([-1 5.5])
    hold on
    yyaxis right
    plot(trans_rate_tout/10)
    ylim([-10 105])
    xline(interp1(CB_tout(:,14),CB_tout(:,1),RFQ(2,4)/10),':');
    xline(interp1(CB_tout(:,14),CB_tout(:,1),RFQ(3,4)/10),':');
    xline(interp1(CB_tout(:,14),CB_tout(:,1),RFQ(4,4)/10),':');
    legend('Ws [MeV]', 'Transmission rate','Location','south')
    legend('Orientation','horizontal')
    xlabel('Cell number')
    title('Beam Dynamics (Toutatis)')
    hold off

end

end