function RMS = design_RMS(RF,RFQ)

load('RMS_shape.mat')
load('phys_constants.mat')

phis_input = RFQ(1,3);
Ws_input = RFQ(1,5);
r0 = RFQ(1,1)/1000;
lambda = c/RF.f;

%% Compute RMS
% z
gamma = (Ws_input*q_p*1e6 + m_p*c^2)/(m_p*c^2);
Beta = sqrt(1-(1/gamma^2));
l = Beta*lambda/2;

Z = [];
RRR = (6*360+60)/(6*360);
for i = [1:6]
    Z(i) = 100* l * (i-7) * RRR;
end
Z(7) = -1e-5;

RMS_array(:,1) = Z';
% B
RMS_array(:,2) = RMS_shape(:,2) * (q_p*70*1000*lambda^2)/(m_p*c^2*r0^2);
% synch_phase
RMS_array(:,3) = ones(7,1) * phis_input;
% synch_phase
RMS_array(:,4) = ones(7,1);
% synch_phase
RMS_array(:,5) = ones(7,1) * RF.V/1000;

%% write RMS string

RMS(1) = join(["zdata -5",RMS_array(1,:)]);
RMS(2) = join(["        ",RMS_array(2,:)]);
RMS(3) = join(["        ",RMS_array(3,:)]);
RMS(4) = join(["        ",RMS_array(4,:)]);
RMS(5) = join(["        ",RMS_array(5,:)]);
RMS(6) = join(["        ",RMS_array(6,:)]);
RMS(7) = join(["        ",RMS_array(7,:)," 6"]);

end