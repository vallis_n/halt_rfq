function [E,RFQ,CB] = optimize_CB(rrrfq, RF, RFQ, plot_flag)
%%  optimize_CB.m determines which RFQ parameters to optimize
%                 calls asses_cb.m and returns an error function

%   Input:
%       rrrfq: Optimization vector   
%       RFQ: Geometrical Description of the RFQ (a,m,phi_s,V_0,z,W_s)
%       RF: RF characteristics of the RF 
%       plot_flag: plots results if active

%   Output:
%       E: error
%       RFQ: Updated RFQ array

%   N. Vallis for LINAC7 @ UPV/EHU � March 2021 
% Change optimization parameters within RFQ


RFQ(5,1:3) = rrrfq;

% Create new CB
CB = sweep(RFQ,RF);

E = assess_CB(CB, RF, RFQ, plot_flag);

end