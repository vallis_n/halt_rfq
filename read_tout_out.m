function [Ws trans_rate] = read_tout_out()

%% READ toutatis.out FILE

fid = fopen('Toutatis/toutatis.out');
trigger = " ## z(m) gama-1 x0 y0 p0 x'0 y'0 W0 SizeX SizeY SizeP sxx' syy' spW ex ey ep hx hy hp npart kx ky kz ex99 ey99 ep99 phiS Ws SCCurrent Aper e4D EmitR kr Powlost";

tline = fgetl(fid);
while ischar(tline)
    tline = fgetl(fid);
    if contains(tline,trigger)
        break
    end
end

Ws = [];
trans_rate = [];
N_cells = 1;
tline = fgetl(fid);
while ischar(tline)
    tline = strsplit(tline);
    trans_rate(N_cells) = str2num(cell2mat(tline(23)));
    Ws(N_cells) = str2num(cell2mat(tline(11)));
    N_cells = N_cells+1; 
    tline = fgetl(fid);
end

end