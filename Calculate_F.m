function F = Calculate_F(r0, ro, k, R0, plot)

% Get PO list
PO_list = Get_PO_list(r0, ro, k, R0);

% Write .AF input file
fileID = fopen('SUPERFISH/750_RFQ.AF');

trigger = 'TRIGGER';
security_trig = 2000;
buffer_size = 0;

while buffer_size<security_trig
    line = fgetl(fileID);
    buffer_size = buffer_size+length(line);
    if contains(num2str(line),trigger)
        buffer_size = buffer_size+length(line)+15;
        break
    end

end

fclose(fileID);
fileID = fopen('SUPERFISH/750_RFQ.AF');
buffer = fread(fileID,buffer_size);
fclose(fileID);
fileID = fopen('SUPERFISH/750_RFQ.AF','w');
fwrite(fileID,buffer);
fprintf(fileID,'%s\n',PO_list);
fclose(fileID)

% Execute AUTOFISH
system('C:\Users\nyquist\Desktop\CS_Optimizer\SUPERFISH\750_RFQ.AF')

% Read OUTAUT.TXT file
security_trig = 2000;
fileID = fopen('SUPERFISH/OUTFIS.TXT');

while buffer_size<security_trig
    line = fgetl(fileID);
    if contains(num2str(line),'Final frequency')
        freq = line;
        break
    end
end

freq = split(freq,' = ');
freq = freq(3);
freq = freq{1};
disp(['The infinite cavity resonates at ' freq ' MHz'])
F = str2num(freq);


% Execute .T35 FILE for visualization
if plot
    system('C:\Users\nyquist\Desktop\CS_Optimizer\SUPERFISH\750_RFQ.T35')
end



end

