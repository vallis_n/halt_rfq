function [E, Es] = assess_CB(CB, RF, RFQ, plot_flag)
%%  evaluate_CB.m assesses the quality of a RFQ beam dynamics design 

%   Input:
%       CB: Pteq, Toutatis or analytical channel block     
%       RFQ: Geometrical Description of the RFQ (a,m,phi_s,V_0,z,W_s)
%       RF: RF characteristics of the RF 
%       plot_flag: plots results if active

%   Output:
%       E: Error

%   N. Vallis for LINAC7 @ UPV/EHU � March 2021 � 

load('phys_constants.mat')

%% COMPUTE MERIT FIGURES

% Output Energy
OUT_W_s = CB(end,3);

% Focusing Strength
B_out = CB(end,12);

% Kilpatrick factor
Es = {};
Es.lim = kilpatrick(RF.f/1e6);
Es.alpha = 1.36;
Es.field = Es.alpha*RF.V/1e3./(CB(:,10)/1e2);
Es.K_factor = Es.field/Es.lim;
Es.max_K_factor = max(Es.K_factor);

%% error
w1 = 4;
w2 = 4;
w3 = 0.5;

E = w1*(OUT_W_s-5)^2 + w2*(Es.max_K_factor-1.5)^2 + w3*(B_out-3)^2;

%% PLOT
if plot_flag
    disp(['The RFQ has ' num2str(CB(end,1)) ' cells.'])
    disp(['Output Energy: ' num2str(OUT_W_s) ' MeV'])
    disp(['RFQ length: ' num2str(CB(end,14)*10) ' mm'])
    disp(['Length of last cell: ' num2str(CB(end,13)*10) ' mm'])
    disp(['Vane Voltage: ' num2str(CB(end,2)) ' kV'])
    disp(['Maximum Kilpatrick factor: ' num2str(Es.max_K_factor)])

    
    figure(1)
    plot(CB(:,14)*10,CB(:,3))
    hold on
    plot(CB(:,14)*10,CB(:,12))
    plot(CB(:,14)*10,Es.K_factor)
    xline(RFQ(2,4));
    xline(RFQ(3,4));
    xline(RFQ(4,4));
    legend('Ws [MeV]', 'Focusing Strength', 'Kilpatrick Factor')
    xlabel('z [mm]')
    title('RFQ quality assessment')
    hold off

    
    figure(2)
    plot(CB(:,14)*10,CB(:,8)*10)
    hold on
    plot(CB(:,14)*10,CB(:,9))
    ylim([0 4])
    yyaxis right
    plot(CB(:,14)*10,CB(:,7),'-','Color','#EDB120')
    ylim([-40 -5])
    xline(RFQ(2,4));
    xline(RFQ(3,4));
    xline(RFQ(4,4));
    legend('a [mm]', 'm', 'phi_s [deg]')
    xlabel('z [mm]')
    title('RFQ modulation Parameters')
    hold off
    drawnow
    
    figure(3)
    plot(CB(:,1),CB(:,3))
    hold on
    plot(CB(:,1),CB(:,12))
    plot(CB(:,1),Es.K_factor)
    legend('Ws [MeV]', 'Focusing Strength', 'Kilpatrick Factor')
    xlabel('z [mm]')
    title('RFQ quality assessment')
    hold off

    
    figure(4)
    plot(CB(:,1),CB(:,8)*10)
    hold on
    plot(CB(:,1),CB(:,9))
    ylim([0 4])
    yyaxis right
    plot(CB(:,1),CB(:,7),'-','Color','#EDB120')
    ylim([-40 -5])
    legend('a [mm]', 'm', 'phi_s [deg]')
    xlabel('z [mm]')
    title('RFQ modulation Parameters')
    hold off
    drawnow

    

end
end