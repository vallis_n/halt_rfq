function [alpha,beta,gamma] = root3d(x)

alpha = (x(2)/x(1)-x(1)/x(2))*sind(x(3))*cosd(x(3));
beta = x(2)/x(1)*sind(x(3))^2 + x(1)/x(2)*cosd(x(3))^2;
gamma = x(2)/x(1)*cosd(x(3))^2 + x(1)/x(2)*sind(x(3))^2;

end