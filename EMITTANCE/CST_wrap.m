clear

c = 299792458; % Speed of light
q_p = 1.602176634e-19; % Proton charge and mass
m_p = 1.67262192369e-27;

load('lebt_out.mat')
P_dist = table2array(LEBT_out);
P_dist(P_dist(:,2) < 800,:) = []; % Remove "halo" particles

P_dist(:,1) = [];
P_dist(:,7) = P_dist(:,1);
P_dist(:,1:2) = P_dist(:,2:3);
P_dist(:,3) = P_dist(:,7)-1000;
P_dist(:,1:3) = P_dist(:,1:3)/1000;

P_dist(:,7) = P_dist(:,4);
P_dist(:,4:5) = P_dist(:,5:6);
P_dist(:,6) = P_dist(:,7);
P_dist(:,4:6) = P_dist(:,4:6)*1000*m_p;

P_dist(:,7) = m_p*ones(length(P_dist),1);
P_dist(:,8) = q_p*ones(length(P_dist),1);
P_dist(:,9) = 10e-6*ones(length(P_dist),1);

fid=fopen('part_dist.pid','w');

for iii = 1:length(P_dist)
    fprintf(fid,'%s\n',num2str(P_dist(iii,:)));
end

fclose('all');