clear all
clc

%% IMPORT DATA
Beta_rel = 0.009233507564542;
load('lebt_out.mat')
P_dist = table2array(LEBT_out);
P_dist(P_dist(:,2) < 800,:) = []; % Remove "halo" particles

size_plot = size(P_dist(:,1));

X = P_dist(:,3)/1000; % x in m
Y = P_dist(:,4)/1000; % y in m
X_prime = atan(P_dist(:,6)./P_dist(:,5)); % x' in rad
Y_prime = atan(P_dist(:,7)./P_dist(:,5)); % y' in rad

%% APPROXIMATE BEAM PROFILE

%M = 0.021;
%ecc = 0.999;
 M = 0.021;
 ecc = 0.9994;
N = sqrt(1-ecc^2)*M;
Ksi = 92.5;

RMS_x = ellipse1(0,0,[M ecc],Ksi);

eps = M*N;

beta = max(RMS_x(:,1))^2/eps;
gamma = max(RMS_x(:,2))^2/eps;
[NaN Xp_x_max] = max(RMS_x(:,1));
Xp_x_max = RMS_x(Xp_x_max,2);
alpha = -Xp_x_max/sqrt(eps/beta);

elip_f = @(x,y) gamma*x^2 + 2*alpha*x*y + beta*y^2;

part_in = [];
in_count = 0;
for iii = 1:length(X)
    if elip_f(X(iii),X_prime(iii)) < eps
        in_count = in_count + 1;
        part_in(in_count,:) = [X(iii) X_prime(iii)];
    end
end

disp(length(part_in)/length(X)*100)

eps_cm_rad = eps*1e2;
eps_pi_mm_mrad = eps*1e6*Beta_rel;

Pteq = [alpha beta*100 eps*100]

%% PLOT BEAM PROFILES
figure(1);

subplot(1,2,1)
hist3([X*1000,X_prime*1000],'Nbins',[250 250],'CdataMode','auto','EdgeColor','none')
hold on
plot3(1000*RMS_x(:,1),1000*RMS_x(:,2),500*ones(length(RMS_x),1),'LineWidth',2,'Color','r')
hold off
xlabel('x [mm]')
ylabel("x' [mrad]")
xlim([-2 2])
ylim([-20 20])
view(2)

subplot(1,2,2)
scatter(1000*X,1000*X_prime,ones(length(X),1),'r')
hold on
scatter(1000*part_in(:,1),1000*part_in(:,2),ones(length(part_in),1),'g')
plot3(1000*RMS_x(:,1),1000*RMS_x(:,2),500*ones(length(RMS_x),1),'--','LineWidth',2,'Color','k')
xlabel('x [mm]')
ylabel("x' [mrad]")
xlim([-4 4])
ylim([-25 25])
hold off