%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% High Acceleration - Low Transmission RFQ Designer
%
% N. Vallis for LINAC7 @ UPV/EHU � June 2021 � 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear 
clc
load('phys_constants.mat')
addpath('Pteq')

%% INPUT 

RF = {}; % RFQ Operation Charcateristics
RF.f = 750e6; RF.lambda = c/RF.f; RF.V = 70; RF.i = 0.3;
RF.emit = [1.2561    8.9503    0.0015];
RF.cb1 = 2; RF.rho_r0_ratio = 1;

% RMS data is stored at "RMS.txt"

% RFQ = [ a[mm]  m   phi_s[deg]   z[mm]   V[kV]   W_s[MeV] ]

RFQ = [2        1       -30     0       0.04; ...      %IN
       2        1.2     -20     350     0.05; ...      %Sh
       1        3       -15     600     0.5; ...       %GB
       1        3       -15     2000    5];            %OUT

%% OPTIMIZE
% 
 rrrfqini = [350,600];
%  lb = [200,400];
%  ub = [500,750];
% 
%  options=optimoptions('fmincon');
%  options.Display='iter','fval';
%  
%  rrrfqopt = fmincon(@(rrrfq) get_RFQ_error(rrrfq,RF,RFQ), ...
%                     rrrfqini, [], [], [], [], lb, ub, [], options);

% [E,trans_rate] = get_RFQ_error(rrrfqini,RF,RFQ);
trans_rate = RFQ_solver(RF,RFQ,1);




