function [l_c,E,delta_phi,k,A,Delta_W_s] = get_cell_length(Beta,f,phi_s,z,L,mmm,aaa,V_0)
load('phys_constants.mat')
v_0 = Beta*c;
lambda = c/f;

lllini = Beta*lambda/2; % first approximation

if z+20 >= L;
    [l_c,E,delta_phi,k,A,Delta_W_s] = [0,0,0,0,0,0];
    
else   
    options=optimoptions('fminunc');
    options.Display='off';

    l_c = fminunc(@(lll) cell_length_error(z,lll,phi_s,f,v_0,mmm,aaa,V_0),lllini,options);
    
    [E,delta_phi,k,A,Delta_W_s] = cell_length_error(z,l_c,phi_s,f,v_0,mmm,aaa,V_0);
    disp(num2str(delta_phi))
end

end