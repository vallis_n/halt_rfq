function [CB_tout,Es] = pteq2tout(RFQ,RF,output_file_flag)
load('RMS.mat')

%% LOAD DATA FROM PARIOUT.TXT
fid = fopen('Pteq/PARIOUT.TXT');
trigger = " Cell    V     Wsyn    Beta    E0      A10     Phi     a       m       r0     Rho      B       L       Z     Imax,T  Imax,L";

tline = fgetl(fid);

while ischar(tline)
    tline = fgetl(fid);
    if tline == trigger
         tline = fgetl(fid);
        break
    elseif contains(tline,'Total length of fringe-field region')
        GapFFS = erase(tline,'Total length of fringe-field region = ');
        GapFFS = erase(GapFFS,' cm');
        GapFFS = str2num(GapFFS)/100;
    end
end

N_cells = 1;
while ischar(tline)
    tline = fgetl(fid);
    if contains(tline,'F')
        break
    end
    tline(regexp(tline,'R')) = '';
    tline(regexp(tline,'T')) = '';
    tline = strsplit(tline);
    tline(1) = [];
    for i = 1:16;
        CB_tout(N_cells,i) = str2num(cell2mat(tline(i)));
    end
    tline = cell2mat(tline);
    N_cells = N_cells+1;
end
% CB_tout(:,1) = CB_tout(:,1)-1;
N_cells = N_cells-1;

% Kilpatrick factor
Es = {};
Es.lim = kilpatrick(RF.f/1e6);
Es.alpha = 1.36;
Es.field = Es.alpha*RF.V/1e3./(CB_tout(:,10)/1e2);
Es.K_factor = Es.field/Es.lim;
Es.max_K_factor = max(Es.K_factor);

%% HEADER
N_RMS_cells = length(RMS)-1;

header = ["DatFlag 1"; ...
          "theAcceleratedFlag 1"; ...
          "theLossesCriteriaFlag 0"; ...
          "compression_factor -1"; ...
          "theGeometryFileFlag 0 0 "; ...
          "theRandomFlag 0"; ...
          "WallAperture 0.008"; ... inner radius at input [m]
          "SemiWidthWall 0.025"; ... withd of end flange [m]
          "diaphragme 1."; ... cuts the beam transversally [radius in m]
          "theStartingModulation -1"; ...
          "theRhoR0RatioInRMS 0"; ... change manually
          join(["GapRMS",num2str(RF.cb1/1000)]); ... param cb1 [m] (exitffl is output)
          join(["GapFFS",num2str(RF.cb1/10 + GapFFS*100)]); ... cb1 + FFS length [m]
          "thespacechargeperiod 1"; ...
          "theextfieldperiod 1"; ...
          "theTrancellFlag 0"; ...
          "theSpeciesFlag 0"; ...
          " "; ...
          "theDistribution k"; ...
          join(["NbRMSCell", num2str(N_RMS_cells)]); ...
          "theBreakOutAngle 0.0"; ...
          "theAccuracyFlag 3"; ...
          "NStep 8"; ...
          "End"; ... ------------------------------------------------------
          " "; ...
          "RHO 0.85"; ...
          join(["linac 1",num2str(RFQ(1,5)),num2str(RF.f/1e6),"1.00727646688  1"]); ...
          "trancell"; ...
          join(["{ } Tank 1 Length= ll cm,",num2str(N_cells),"cells"]); ...
          " Cell   cl      tl     curlt   curll"];
      
%% NEW CHANNEL BLOCK
% CB_tout(:,14) = CB_tout(:,14) + 0.2;
% CB_tout(1,13) = CB_tout(1,13) + 0.2;
CB_write = join(string(CB_tout));

%% FOOTER
footer = ["rfqout 1"; ...
          " "; ...
          join(["input -6 1000 " num2str(RF.emit) " " num2str(RF.emit) " 180 0"]); ...
          "scheff 0.0"; ...
          join(["exitffl",num2str(GapFFS)]); ... length of FFS
          "vfac 1.0";...
          "end"];

%% WRITE RFQ.inp FILE
if output_file_flag
    
    fid = fopen('Toutatis/RFQ.inp','w');
    fprintf(fid,'%s\r\n',header);
    fprintf(fid,'%s\r\n','0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ');
    fprintf(fid,'%s\r\n',CB_write);
    fprintf(fid,'%s\r\n',footer);
    ST = fclose('all');
    
%    disp('RFQ.inp file successfully created. Ready to launch Toutatis.')
end

end