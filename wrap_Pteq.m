function CB_Pteq = wrap_Pteq(CB, RF, RFQ, output_file_flag)
%%  wrap_Pteq writes the Parmteq input file (RFQ.IN4)
%       Based in a given analytical approximation

%   Input:
%       CB: Analytically approaximated channel block
%       RF: RF characteristics of the RF 
%       RFQ: Geometrical Description of the RFQ (a,m,phi_s,V_0,z,W_s)
%       output_file_flag: Write RFQ.IN4 if on

%   Output:
%       CB_Pteq: RFQuick Channel Block (5 columns)

%   N. Vallis for LINAC7 @ UPV/EHU � June 2021 � 

load('phys_constants.mat')
load('RMS.mat')

%% HEADER

% Special lines
header1 = "*****750 MHz Beam Dynamics Test - N.Vallis 2021*****";

header2 = ["linac 1",num2str(RFQ(1,5)),num2str(RF.f/1e6),"1.00727646688  1"];

header3 = ["tank 1",num2str(RFQ(end,5)),"; Wout(MeV)"];


% Write header
header_lines = ["title"; ...
                join(header1,""); ... 
                "TRANCELL"; ...
                join(header2); ...
                join(header3)];

%% RMS

RMS = design_RMS(RF,RFQ);
            
%% PTEQ channel block

% Enquiry points
enquiry_points = [];

for i = 1:length(RFQ(:,1))-2
    enquiry_points = [enquiry_points RFQ(i,4):(RFQ(i+1,4)-RFQ(i,4))/10:RFQ(i+1,4)];
    enquiry_points(end) = [];
end

enquiry_points = [enquiry_points RFQ(end-1,4) RFQ(end,4)]/10;

% Pteq channel block
CB_Pteq = [enquiry_points' ...
      interp1(CB(:,14),CB(:,12),enquiry_points') ...
      interp1(CB(:,14),CB(:,7),enquiry_points') ...
      interp1(CB(:,14),CB(:,9),enquiry_points') ...
      interp1(CB(:,14),CB(:,2)/1e3,enquiry_points')];
  
CB_Pteq(1,:) = [0 CB(1,12) CB(1,7) CB(1,9) CB(1,2)/1e3];
  
% Write cb
CB_lines = strings([length(enquiry_points),1]);

for i = 1:length(enquiry_points)
    if i == 1
        CB_lines(i) = join(["zdata -5",num2str(CB_Pteq(i,:))]);
    elseif i == length(enquiry_points)
        CB_lines(i) = join([num2str(CB_Pteq(i,:)),"-1"]);
    else
        CB_lines(i) = join([num2str(CB_Pteq(i,:))," "]);
    end
end


%% FOOTER

footer1 = ["elimit " num2str(RF.elimit)];
footer2 = ["input -6 1000 " num2str(RF.emit) " " num2str(RF.emit) " 180 0"];

% Write footer
footer_lines = ["rfqout"; ...
                "start 1"; ...
                "stop -1"; ...
                join(footer1,""); ... 
                join(footer2,"");];



%% Write RFQ.INI file
if output_file_flag
    
    load('RFQ_INI.mat')

    fid = fopen('Pteq/RFQ.IN4','w');
    fprintf(fid,'%s\r\n',header_lines);
    fprintf(fid,'%s\r\n',RMS);
    fprintf(fid,'%s\r\n',CB_lines);
    fprintf(fid,'%s\r\n',footer_lines);
    fprintf(fid,'%s\r\n',RFQ_INI.default_lines);
    ST = fclose('all');
    
%    disp('RFQ.IN4 file successfully created. Ready to launch Parmteq.')
end


end