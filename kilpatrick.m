function E_k = kilpatrick(f)

Error = @(eee_k) (f - 1.643 * eee_k^2 * exp (-8.5/eee_k))^2;

options_k.Display = 'off';

E_k = fminunc(Error,25,options_k);

end
