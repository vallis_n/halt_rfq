function [E,trans_rate,RFQ] = get_RFQ_error(rrrfq, RF, RFQ)
%%  get_RFQ_error gets the error of RFQ transmission rate
%       by averaging RFQ_solver.m function results

%   Input:
%       rrrfq: optimization vector
%       RF: RF characteristics of the RF 
%       RFQ: Geometrical Description of the RFQ (a,m,phi_s,V_0,z,W_s)

%   Output:
%       E: Error
%       trans_rate: Transmission rate [%]


%   N. Vallis for LINAC7 @ UPV/EHU · June 2021

%% ALLOCATE OPT. PARAMS. 
RFQ(2,4) = rrrfq(1);
RFQ(3,4) = rrrfq(2);

%% SWEEP 
ttt = [];
for i = 1:10 % change here no. of evaluations
    ttt(i) = RFQ_solver(RF,RFQ,0); 
end

trans_rate = mean(ttt);

E = (trans_rate - 40)^2; % change here objective function

end