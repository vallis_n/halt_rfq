function CB = Acceleration_sweep(rrrfq, RFQ, RF)

load('phys_constants.mat')

z = RFQ(3,4); % [mm]
V_0 = RFQ(3,5)*1e3; % [V]
W_s = RFQ(3,6)*q_p*1e6; % [J]
RFQ_l = RFQ(4,4);

f = RF.f;
lambda = RF.lambda;

N = 0;
Delta_W_s = 0;

aaa = rrrfq(1)/1000; % [mm]
mmm = rrrfq(2);
ppphi_s = rrrfq(3); % [deg]

while z<RFQ_l
    
    % Compute Channel Block
    W_s = W_s + Delta_W_s; % POS 3 Energy at the beginning of cell
    gamma = (W_s + m_p*c^2)/(m_p*c^2);
    Beta = sqrt(1-(1/gamma^2));      % normalized speed of synchronous particle
    l_c = lambda*Beta/2; % POS 13. delta_phi = 0 approximation.
    k = pi/l_c;
    
    N = N+1; % POS 1
    % V = ?; % POS 2
    % POS 3 is W_s
    % POS 4 is Beta
    A = (mmm^2 - 1) / ...
        (mmm^2 * besseli(0,k*aaa) + besseli(0,k*mmm*aaa)); % POS 6
    E_0 = 2*A*V_0/(Beta*lambda); % POS 5
    % POS 7,8,9 are ppphi_s, aaa, mmm
    r_0 = (aaa + mmm*aaa)/2; % POS 10 Straight line approximation
    rho = r_0; % POS 11 change!!!
    X = (besseli(0,k*aaa)+besseli(0,k*mmm*aaa))/((mmm^2)*(besseli(0,k*aaa))+besseli(0,k*mmm*aaa));
    B = (q_p/m_p)*(V_0/aaa)*(1/f^2)*(1/aaa)*X;
    % POS 13 is cell length
    z = z+l_c*1000; % POS 14
    
    % Energy gain
    Delta_W_s = q_p * (pi/4) * A * V_0 * cosd(ppphi_s);
    
    % Write Channel Block
    CB(N,:) = [N V_0/1e3 W_s/q_p/1e6 Beta E_0 A ppphi_s aaa*100 mmm r_0*100 rho/10 B l_c*100 z/10 X k];
    
end
end
