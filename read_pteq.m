function trans_rate = read_pteq()
%%  read_pteq.m reads the PARMTEQOUT.txt file
%       gets a discrete value of the transmission rate at the output

%   Output:
%       trans_rate: Beam transmission rate [%]

%   N. Vallis for LINAC7 @ UPV/EHU · June 2021 ·

%% READ PARMTEQOUT.txt FILE

fid = fopen('Pteq/PARMTEQOUT.TXT');
trigger = "End exit fringe field section";

tline = fgetl(fid);
while ischar(tline)
    tline = fgetl(fid);
    if contains(tline,trigger)
        break
    end
end

tline = fgetl(fid);
tline = strsplit(tline);

trans_rate = str2num(cell2mat(tline(6)))/10;

end