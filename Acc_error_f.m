function [E,CB] = Acc_error_f(rrrfq, RF, RFQ, plot_acc)

load('phys_constants.mat')

%% SWEEP ACC. CELLS

CB = Acceleration_sweep(rrrfq, RFQ, RF);


%% Display results

% Output Energy
OUT_W_s = CB(end,3);

% Focusing Strength
B_out = CB(end,12);

% Kilpatrick factor
Es = {};
Es.lim = kilpatrick(RF.f/1e6);
Es.alpha = 1.36;
Es.field = Es.alpha*RFQ(3,5)./CB(:,10)/1e2;
Es.K_factor = Es.field/Es.lim;
Es.max_K_factor = max(Es.K_factor);

%% error
w1 = 4;
w2 = 4;
w3 = 0.5;

E = w1*(OUT_W_s-5)^2 + w2*(Es.max_K_factor-1.5)^2 + w3*(B_out-3)^2;

%% PLOT
if plot_acc
    disp('Accelerating section computed w/ the following parameters')
    disp(['a = ' num2str(rrrfq(1)) ' mm, m = ' num2str(rrrfq(2)) ', phi_s = ' num2str(rrrfq(3)) ' deg'])
    disp(['Acc. section has ' num2str(CB(end,1)) ' cells.'])
    disp(' ')
    disp('Beam at OUTPUT:')
    disp(['Synchronous velocity is ' num2str(OUT_W_s) ' MeV'])
    disp(['Focusing Force is ' num2str(B_out) ' MeV'])
    disp(['Maximum Kilpatrick factor is ' num2str(Es.max_K_factor)])

    
    figure(1)
    plot(CB(:,14),CB(:,3)/q_p/1e6)
    hold on
    yyaxis right
    plot(CB(:,14),CB(:,12))
    plot(CB(:,14),Es.K_factor)
    legend('Ws [MeV]', 'Focusing Strength', 'Kilpatrick Factor')
    xlabel('z [m]')
    title('RFQ modulation Parameters')
    hold off
    

end
end
