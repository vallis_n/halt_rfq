function [CB,RF] = sweep(RFQ,RF)
%%  sweep.m creates an analytical approximation of a given RFQ
%       Creates a Channel Block suitable for Parmteq and Toutatis

%   Input:
%       RFQ: Geometrical Description of the RFQ (a,m,phi_s,V_0,z,W_s)
%       RF: RF characteristics of the RF 

%   Output:
%       CB: Channel Block (Ncells x 16 array)
%       RF: Updated 'RF' array

%   N. Vallis for LINAC7 @ UPV/EHU � March 2021 � 

%% INPUT 

load('phys_constants.mat')

% Global RFQ Characteristics
RFQ_l = RFQ(end,4); % mm
V = RF.V * 1e3; % V
f = RF.f; 
lambda = RF.lambda; 
RFQ_sections = []; a = []; m = []; phi_s = [];

for i = 1:length(RFQ(:,1))
    RFQ_sections(i) = RFQ(i,4);
    a(i) = RFQ(i,1)/1000;
    m(i) = RFQ(i,2);
    phi_s(i) = RFQ(i,3);
end

% Initial params.
z = RFQ(1,4); % mm
N = 6; % 1st cell after RMS
Ws = RFQ(1,5)*q_p*1e6;
Delta_Ws = 0;

CB = [];

while z < RFQ_l % loop
    aaa = interp1(RFQ_sections,a,z);
    mmm = interp1(RFQ_sections,m,z);
    ppphis = interp1(RFQ_sections,phi_s,z);
    
    Ws = Ws + Delta_Ws;
    gamma = (Ws + m_p*c^2)/(m_p*c^2);
    Beta = sqrt(1-(1/gamma^2));
    l = Beta*lambda/2;
    k = pi/l;
    
    N = N+1; % POS 1
    % V = ?; % POS 2
    % POS 3 is W_s
    % POS 4 is Beta
    A = (mmm^2 - 1) / ...
        (mmm^2 * besseli(0,k*aaa) + besseli(0,k*mmm*aaa)); % POS 6
    E_0 = 2*A*V/(Beta*lambda); % POS 5
    % POS 7,8,9 are ppphi_s, aaa, mmm
    rrr0 = (aaa + mmm*aaa)/2; % POS 10 Straight line approximation
    rho = rrr0; % POS 11 change!!!
    X = (besseli(0,k*aaa)+besseli(0,k*mmm*aaa))/((mmm^2)*(besseli(0,k*aaa))+besseli(0,k*mmm*aaa));
    B = (q_p*V*lambda^2)/(m_p*c^2*rrr0^2); % POS 12
    % POS 13 is cell length
    z = z+l*1000; % POS 14
    % CURRENT LIMITS (POS 15 & 16) NOT INCLUDED
    
    Delta_Ws = q_p * (pi/4) * A * V * cosd(ppphis); 
    
    CB(N-6,:) = [N V/1e3 Ws/q_p/1e6 Beta E_0 A ppphis aaa*100 mmm rrr0*100 rho/10 B l*100 z/10 0 0];
    
end  

RF.elimit = sqrt((2 * q_p * E_0 * (pi/4) * Beta^3 * gamma^3 * lambda) ...
               * (deg2rad(ppphis) * cosd(ppphis) - sind(ppphis)) ...
               / (pi * m_p * c^2)) * m_p * c^2 / q_p / 1e6;


end
