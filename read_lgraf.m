function [trans_rate, Ws] = read_lgraf();
%%  read_pteq.m reads the LGRAFOUT.txt file
%       gets an array value of the trans rate and synch energy

%   Output:
%       trans_rate: Beam transmission rate [%]
%       Ws: Synch. energy [MeV]


%   N. Vallis for LINAC7 @ UPV/EHU · June 2021 ·

%% READ LGRAFOUT.TXT FILE

fid = fopen('Pteq/LGRAFOUT.TXT');
trigger = "  cell  plot  ngood    zrms      z90     z100    dPhi_av  dW_av   alphaz    betaz          Z         W";

tline = fgetl(fid);
while ischar(tline)
    tline = fgetl(fid);
    if contains(tline,trigger)
        break
    end
end

trans_rate = [];
N_cells = 1;
tline = fgetl(fid);
while ischar(tline)
    tline = strsplit(tline);
    trans_rate(N_cells) = str2num(cell2mat(tline(4)))/10;
    Ws(N_cells) = str2num(cell2mat(tline(end)));
    N_cells = N_cells+1; 
        tline = fgetl(fid);
    if contains(tline,"LGF: end")
        break
    end
end

end